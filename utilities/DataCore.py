class DataCore:
    def __init__(self):
        self.world = None
        self.robot = None

    def set_world(self, world):
        self.world = world

    def get_world(self):
        if self.world is not None:
            return self.world
        else:
            print("World is not set")
            return None

    def set_robot(self, robot):
        self.robot = robot

    def get_robot(self):
        if self.robot is not None:
            return self.robot
        else:
            print("Robot is not set")
            return None