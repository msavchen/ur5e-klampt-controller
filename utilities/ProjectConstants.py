import os
import math


class ProjectConstants(object):
    HARDWARE = True

    CONTROLLER_DT = .005
    # Starting config for ur5.rob file
    STARTING_CONFIG = [0.0, 0.0, -1.5707963267948966, 0.0, -1.5707963267948966, 0.0, 0.0, 0.0]
    HOME_CONFIG = None

    END_EFFECTOR_IND = 6

    # DIRECTORY LIST
    dir_path = os.path.dirname(os.path.realpath(__file__))

    world_file = dir_path + "/../data/worlds/" + "flatworld" + ".xml"
    robot_file = dir_path + "/../data/robots/ur5e.rob"

    robot_ip_adress = "192.168.0.106"

