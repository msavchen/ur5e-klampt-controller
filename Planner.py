import math
import time
import numpy as np
from heapq import heappop, heappush, heapify, nsmallest

import klampt
from klampt import *
from klampt import vis
from klampt.model import coordinates
from klampt.model import ik, collide
from klampt.math import vectorops, so2, so3
from klampt.vis import GLRealtimeProgram
from klampt.model.collide import WorldCollider
from klampt.model.trajectory import Trajectory
from klampt.model.trajectory import RobotTrajectory
from klampt.plan import cspace, robotplanning, robotcspace

from utilities.ProjectConstants import ProjectConstants
from robot_api.RobotController import UR5WithGripperController


class Planner:

    def __init__(self):
        world_file = ProjectConstants.world_file
        robot_file = ProjectConstants.robot_file
        solo_cup_obj = ProjectConstants.solo_cup_obj
        drawer_obj = ProjectConstants.drawer_obj

        self.world = self.get_world(world_file, robot_file, visualize_robot=True)
        self.robot = self.world.robot(0)
        ProjectConstants.HOME_CONFIG = self.robot.getConfig()
        self.solo_cup = self.world.loadRigidObject(solo_cup_obj)
        self.drawer = self.world.loadRigidObject(drawer_obj)

        self.place_item('cup', [2, 2, 2])
        self.place_item('drawer', [2, 2, 2])

        self.visualize_world(self.world)

        self.currently_planning = False
        self.executing_trajectory = None

        self.ur5 = self.initialize_robot()
        self.home = True

        self.placing_object = True
        self.planning_object = False
        self.picking_object = False

        self.calibration_config_index = 0

        # print "a"
        # self.current_config = ProjectConstants.STARTING_CONFIG

    def start(self):
        self.ur5.start()

    def initialize_robot(self):
        # Needs to start ur5.start()
        # and shut_down ur5.stop()
        try:
            ur5 = UR5WithGripperController(ProjectConstants.robot_ip_adress, gripper=False, gravity=[0, 0, 9.8])

        except:
            print "UR5 connection is not initalized!"
            ur5 = None

        return ur5

    def execute_trajectory(self, input_trajectory, speed=ProjectConstants.CONTROLLER_DT, vis=True, hardware=False):
        if hardware:
            self.ur5.start()
            ProjectConstants.HOME_CONFIG = self.ur5_to_klampt(self.ur5.getConfig())

        self.executing_trajectory = True
        st = time.time()
        ct = time.time()
        while ct - st < ProjectConstants.ARM_EXECUTION_TIME + ProjectConstants.HAND_EXECUTION_TIME:
            config = input_trajectory.eval(ct - st)
            if vis:
                self.robot.setConfig(config)
            if hardware:
                ur5_config = self.klampt_to_ur5(config)
                self.ur5.setConfig(ur5_config)
            time.sleep(speed)
            ct = time.time()
        if hardware:
            self.ur5.stop()
            print "AFTER STOP"
            self.ur5 = self.initialize_robot()
        self.executing_trajectory = False

    def return_home(self):
        end_config = ProjectConstants.HOME_CONFIG
        init_config = self.robot.getConfig()

        space = self.generate_space(self.world, self.robot)
        self.validate_config(space, [init_config, end_config], silent=False)
        arm_path = self.motion_planner(space, init_config, end_config)
        arm_path_time = np.linspace(0, ProjectConstants.ARM_EXECUTION_TIME, len(arm_path)).tolist()
        traj = Trajectory(times=arm_path_time, milestones=arm_path)

        # print traj
        # self.currently_planning = False
        return traj

    def generate_trajectory(self, point, grip=True):
        # self.currently_planning = False

        init_config = self.robot.getConfig()
        space = self.generate_space(self.world, self.robot)
        heap = []

        for i in range(1000):
            self.robot.setConfig(init_config)
            rand_point_config = self.solve_ik(space, point)
            if rand_point_config is not None:
                diff = [so2.diff(g, s) for g, s in zip(rand_point_config, init_config)]
                cost = np.linalg.norm(diff)
                heappush(heap, (cost, rand_point_config))

        if len(heap) == 0:
            print "No configuration for the cup found"
            self.robot.setConfig(init_config)
            return None

        print "FOUND ", len(heap), " configurations"
        for qgoal in nsmallest(5, heap):
            end_config = qgoal[1]
            self.robot.setConfig(init_config)
            arm_path = self.motion_planner(space, init_config, end_config)
            if arm_path is not None:
                arm_path_time = np.linspace(0, ProjectConstants.ARM_EXECUTION_TIME, len(arm_path)).tolist()
                traj = Trajectory(times=arm_path_time, milestones=arm_path)
                return traj

        self.robot.setConfig(init_config)
        return None

    def path_into_timed_traj(self, path):
        path_time = np.linspace(0, ProjectConstants.ARM_EXECUTION_TIME, len(path)).tolist()
        traj = Trajectory(times=path_time, milestones=path)
        return traj

    def gen_had_traj(self, space, dict):
        robot = self.robot
        qold = robot.getConfig()
        milestone = 55
        # set goal of hand configuration
        robot.driver(dict.get("gripper:swivel_1")).setValue(0)
        robot.driver(dict.get("gripper:proximal_1")).setValue(1.4)
        robot.driver(dict.get("gripper:proximal_2")).setValue(1.4)
        robot.driver(dict.get("gripper:proximal_3")).setValue(1.4)
        qnew = robot.getConfig()
        qmin, qmax = robot.getJointLimits()
        # for i in range(len(qold)):
        #     print i,qold[i],qnew[i],qmin[i],qmax[i]
        # raw_input()

        sub = []
        for i in range(len(qnew)):
            # sub.append(qnew[i] - qold[i])
            sub.append(so2.diff(qnew[i], qold[i]))

        robot.setConfig(qold)
        path = []
        for i in range(milestone):
            point = []
            for j in range(len(qnew)):
                point.append(qold[j] + sub[j] / (milestone - i))
            path.append(point)

        for i in range(6 * milestone):
            path.append(path[-1])

        # cut_off_config = 0
        # for i in range(len(path)):
        #     if not self.validate_config(space, path[i]):
        #         break
        #     cut_off_config = i
        # return path[:cut_off_config+1]
        return path

    @staticmethod
    def gen_hand_dictionary(robot):
        dict = {}
        for i in range(6, 40):
            dict[robot.driver(i).getName()] = i
        return dict

    @staticmethod
    def motion_planner(gen_space, qstart, qgoal):
        # settings = {'type': 'fmm',
        #             'perturbationRadius': 0.25,
        #             'bidirectional': True,
        #             'shortcut': True,
        #             'restart': True,
        #             'restartTermCond': "{foundSolution:1,maxIters:1000}"
        #             }

        settings = {'type': 'prm'}

        t0 = time.time()
        print "Creating planner..."

        # Manual construction of planner
        planner = cspace.MotionPlan(gen_space, **settings)
        # planner.setEndpoints(qstart, qgoal)
        planner.setEndpoints(qstart, qgoal)

        print "Planner creation time", time.time() - t0
        t0 = time.time()
        print "Planning..."
        numIters = 0
        for round in range(10):
            planner.planMore(500)
            numIters += 1
            if planner.getPath() is not None:
                break

        print "Planning time,", numIters, "iterations", time.time() - t0
        # provide some debugging information
        V, E = planner.getRoadmap()
        print len(V), "feasible milestones sampled,", len(E), "edges connected"

        path = planner.getPath()

        if path is not None:
            print "Got a path with", len(path), "milestones"
            path = []
            for i in range(100):
                point_in_path = gen_space.interpolate(qstart, qgoal, float(i) / 100)
                path.append(point_in_path)
            # gen_space.interpolate(path)
        else:
            print "No feasible path was found"
        # print path
        # time.sleep(10000)
        return path

    def solve_ik(self, space, end_pos, top_grip=True):
        robot = self.robot
        max_iter = 1000

        if top_grip and end_pos[2] <= 0.5:
            end_pos1 = end_pos[:]
            end_pos1[2] += 0.05
            end_pos2 = end_pos[:]
            end_pos2[2] += 0.15
            # t = Trajectory(milestones=[end_pos1, end_pos2])
            # vis.add("tasd", t)
            worldpos = [end_pos1, end_pos2]
            localpos = [[0.125, 0, 0], [0.225, 0.0, 0]]
            localpos = localpos[::-1]
            # a = robot.link(7).getWorldPosition(localpos[0])
            # b = robot.link(7).getWorldPosition(localpos[1])
            # t2 = Trajectory(milestones=[a, b])
            # vis.add("tasdasdasd", t2)
            # raw_input()
        else:
            end_pos1 = end_pos[:]
            end_pos1[2] += 0.05
            end_pos2 = end_pos[:]
            end_pos2[2] += 0.15
            # t = Trajectory(milestones=[end_pos1, end_pos2])
            # vis.add("tasd", t)
            # raw_input()
            worldpos = [end_pos1, end_pos2]
            localpos = [[0.18, 0, 0], [0.18, 0.1, 0]]
            # a = robot.link(7).getWorldPosition(localpos[0])
            # b = robot.link(7).getWorldPosition(localpos[1])
            # t2 = Trajectory(milestones=[a, b])
            # vis.add("tasdasdasd", t2)
            # raw_input()

        obj = ik.objective(robot.link(ProjectConstants.END_EFFECTOR_IND), local=localpos, world=worldpos)

        def feas_check():
            config = self.robot.getConfig()
            return self.validate_config(space, config, silent=True)

        res = ik.solve_global(obj, iters=max_iter, startRandom=True, feasibilityCheck=feas_check)
        # res = ik.solve_global(obj, iters=max_iter, startRandom=True)

        if res:
            end_config = robot.getConfig()
            return end_config
        else:
            return None

    @staticmethod
    def generate_space(world, robot):
        collider = WorldCollider(world)
        objs = []
        objs.append(ik.fixed_objective(robot.link(0)))
        space = robotcspace.ClosedLoopRobotCSpace(robot, objs, collider)
        space.setup()

        return space

    def get_hand_dictionary(self):
        robot = self.robot
        dict = {}
        for i in range(6, 40):
            dict[robot.driver(i).getName()] = i
        return dict

    def get_hand_trajectory(self, dict):  # , space):
        robot = self.robot
        qold = robot.getConfig()
        milestone = 5
        # set goal of hand configuration
        robot.driver(dict.get("gripper:swivel_1")).setValue(0.5)
        robot.driver(dict.get("gripper:proximal_1")).setValue(1.4)
        robot.driver(dict.get("gripper:proximal_2")).setValue(1.4)
        robot.driver(dict.get("gripper:proximal_3")).setValue(1.4)
        qnew = robot.getConfig()
        qmin, qmax = robot.getJointLimits()
        # for i in range(len(qold)):
        #     print i,qold[i],qnew[i],qmin[i],qmax[i]
        # raw_-input()
        sub = []
        for i in range(len(qnew)):
            sub.append(qnew[i] - qold[i])
        robot.setConfig(qold)
        path = []
        for i in range(milestone):
            point = []
            for j in range(len(qnew)):
                point.append(qold[j] + sub[j] / (milestone - i))
            path.append(point)
        for i in range(6 * milestone):
            path.append(path[1])
        return path

    def place_item(self, item, t, R=None):

        if R is None:
            R = [1, 0, 0, 0, 1, 0, 0, 0, 1]
        if item == "cup":
            self.solo_cup.setTransform(R, t)
        elif item == "drawer":
            self.drawer.setTransform(R, t)

    def visualize_world(self, world):
        vis.add("world", world)
        vis.add("coordinates", coordinates.manager())
        vis.setWindowTitle("Height Map GeneratorSuperclass World")
        vp = vis.getViewport()
        vp.w, vp.h = 600, 600
        vis.setViewport(vp)
        vis.autoFitCamera()
        vis.show()

    def get_world(self, world_file, robot_file, visualize_robot=True):
        world = WorldModel()
        res = world.readFile(world_file)
        if not res:
            raise RuntimeError("Unable to load terrain model")
        if visualize_robot:
            res = world.readFile(robot_file)
            print robot_file
            if not res:
                raise RuntimeError("Unable to load robot model")
            current_config = world.robot(0).getConfig()
            # print current_config
            # time.sleep(100000)
            print len(ProjectConstants.STARTING_CONFIG)
            current_config[:len(ProjectConstants.STARTING_CONFIG)] = ProjectConstants.STARTING_CONFIG
            world.robot(0).setConfig(current_config)
        return world

    @staticmethod
    def ur5_to_klampt(config):
        klampt_config = config[:]
        klampt_config.insert(0, 0.0)
        return klampt_config

    @staticmethod
    def klampt_to_ur5(config):
        return config[1:]

    @staticmethod
    def validate_config(space, config, silent=True):
        # TODO: Are the validation the same as in the robosimia
        # TODO: Is self collision detected
        # TODO: Is world object collision detected
        # TODO: Is feasibility detected
        # TODO: Is the config in the closed loop of the provided space
        try:
            for i in range(len(config)):
                if not silent:
                    print("Config #" + str(i))
                    print("space.inbounds(end_config):", space.inBounds(config[i]))
                    try:
                        print("space.closed_loop(end_config):", space.closedLoop(config[i]))
                    except AttributeError:
                        pass
                    print("space.isFeasible(end_config)", space.isFeasible(config[i]))
                    print("space.selfCollision(end_config):", space.selfCollision(config[i]))

                if space.inBounds(config[i]) and space.isFeasible(config[i]) and not space.selfCollision(
                        config[i]) and space.closedLoop(config[i]):
                    pass
                else:
                    return False
            return True

        except TypeError:
            if not silent:
                print("space.inbounds(end_config):", space.inBounds(config))
                try:
                    print("space.closed_loop(end_config):", space.closedLoop(config))
                except AttributeError:
                    pass
                print("space.isFeasible(end_config)", space.isFeasible(config))
                print("space.selfCollision(end_config):", space.selfCollision(config))
            if config is None:
                return False
            if space.inBounds(config) and space.isFeasible(config) and not space.selfCollision(
                    config) and space.closedLoop(config):
                return True
            else:
                return False

    def calibration_configs(self):
        # list of joint configs for each sample
        # list order:0, base, shoulder, elbow, wrist1, wrist2, wrist3
        c1 = [0, -125.79, -69.93, -158.01, -122.62, 2.85, 87.94, 0]
        c2 = [0, -125.81, -34.28, -125.32, -162.86, 2.85, 71.80, 0]
        c3 = [0, -129.09, 7.18, -99.77, -262.51, -44.38, 92.79, 0]
        c4 = [0, -163.61, 3.35, -23.07, -153.09, 73.9, 274.36, 0]
        c5 = [0, -247.01, 0.14, -23.07, -155.88, 115.6, 284.56, 0]
        c6 = [0, -256.3, -25.27, 48.2, -19.47, 243.0, 101.88, 0]
        c7 = [0, -206.59, -0.21, -12.4, -169.01, 91.94, 277.18, 0]
        c8 = [0, -179.51, 7.29, -12.33, -170.96, 80.92, 277.18, 0]
        c9 = [0, -170.77, -75.39, 105.43, -32.74, 295.31, 103.36, 0]
        c10 = [0, -337.44, -187.18, 97.25, -274.54, 111.75, 97.73, 0]
        c11 = [0, -355.39, -177.01, 144.73, -328.7, 136.48, 98.54, 0]
        c12 = [0, -354.65, -118.69, -77.3, -164.72, 113.8, 98.54, 0]
        # a = [0, -8.25, -80.13, -126.86, 205.14, -241.97, -2.27, 0]
        # b = [0, 46.43, -124.92, -75.98, 196.67, -217.22, -2.39, 0]
        # c = [0, 30.66, -118.03, -110.49, 211.84, -219.59, -2.32, 0]
        # d = [0, -24.24, -152.37, -52.20, 193.15, -254.94, -2.41, 0]
        # e = [0, 48.50, -109.58, -117.95, 222.92, -201.84, -2.41, 0]
        # f = [0, 128.29, -109.77, -155.91, 209.49, 131.63, -2.41, 0]
        # g = [0, 225.61, -36.74, -138.69, 88.78, 5.68, -13.27, 0]
        # h = [0, 226.39, -138.46, -143.92, 189.42, 19.81, -13.27, 0]
        # configs = [a, b, c, d, e, f, g, h]
        configs = [c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12]
        for c in range(len(configs)):
            for i in range(len(configs[c])):
                configs[c][i] = np.deg2rad(configs[c][i])

        print self.calibration_config_index
        if self.calibration_config_index < len(configs):
            self.robot.setConfig(configs[self.calibration_config_index])
            self.calibration_config_index += 1
            return self.robot.link(7).getWorldPosition([0, 0, 0])
        else:
            return None

        # rc = []
        # for c in configs:
        #     print len(c)
        #     self.robot.setConfig(c)
        #     rc.append(self.robot.link(7).getWorldPosition([0, 0, 0]))
        #     raw_input()
        #
        # print rc
        # rc = [[-0.1888209015374181, 0.09858911440196177, 0.23759646843027105],
        #       [-0.41281654731800727, -0.37067797247408174, 0.20698788659544798],
        #       [-0.3520238750731796, -0.15552547185860865, 0.09482543949245031],
        #       [-0.5444639642480453, 0.34140098276034747, 0.04611716808784899],
        #       [-0.2691402384449018, -0.25477217187377565, 0.10836438347937292],
        #       [-0.005314070533358906, -0.08118513573903104, 0.0964181387674024],
        #       [0.10536397868830964, -0.16547102203124564, 0.37724143725729464],
        #       [0.2322990555657673, -0.02665205068813683, 0.020607634264590204]]
