import math
import time
import random
import numpy as np
from heapq import heappop, heappush, heapify, nsmallest

import klampt
from klampt import *
from klampt import vis
from klampt.model import coordinates, ik, collide, trajectory
from klampt.math import vectorops, so2, so3, se3
from klampt.vis import GLRealtimeProgram
from klampt.model.collide import WorldCollider
from klampt.model.trajectory import Trajectory
from klampt.model.trajectory import RobotTrajectory
from klampt.plan import cspace, robotplanning, robotcspace

from utilities.ProjectConstants import ProjectConstants
from robot_api.RobotController import UR5WithGripperController

class MasterPlanner:

    def __init__(self):
        world_file = ProjectConstants.world_file
        robot_file = ProjectConstants.robot_file

        self.world = self.get_world(world_file, robot_file, visualize_robot=True)
        self.robot = self.world.robot(0)
        ProjectConstants.HOME_CONFIG = self.robot.getConfig()

        self.visualize_world(self.world)
        self.ur5 = self.initialize_robot()

    def start(self):
        self.ur5.start()
        return

    def execute_trajectory(self, input_trajectory, speed=ProjectConstants.CONTROLLER_DT, vis=True, hardware=False):
        if hardware:
            self.ur5.start()
            ProjectConstants.HOME_CONFIG = self.ur5_to_klampt(self.ur5.getConfig())

        self.executing_trajectory = True
        st = time.time()
        ct = time.time()
        while ct - st < input_trajectory.times[-1]:
            config = input_trajectory.eval(ct - st)
            if vis:
                self.robot.setConfig(config)
            if hardware:
                ur5_config = self.klampt_to_ur5(config)
                self.ur5.setConfig(ur5_config)
            time.sleep(speed)
            ct = time.time()
        if hardware:
            self.ur5.stop()
            print "AFTER STOP"
            self.ur5 = self.initialize_robot()
        self.executing_trajectory = False

    def return_home(self):
        end_config = ProjectConstants.STARTING_CONFIG
        init_config = self.robot.getConfig()
        space = self.generate_space(self.world, self.robot, "collision_space")
        plan_time = time.time()
        arm_path = self.motion_planner(init_config, end_config, space)
        homming_time = time.time() - plan_time
        self.data["plan_home"] = homming_time
        print "Homming Path found in ", homming_time
        traj = None
        if arm_path is not None:
            arm_path_time = np.linspace(0, ProjectConstants.ARM_EXECUTION_TIME, len(arm_path)).tolist()
            traj = Trajectory(times=arm_path_time, milestones=arm_path)
            # traj = trajectory.path_to_trajectory(arm_path, velocities="trapezoidal", speed=0.8)

        return traj

    def path_into_timed_traj(self, path):
        path_time = np.linspace(0, ProjectConstants.ARM_EXECUTION_TIME, len(path)).tolist()
        traj = Trajectory(times=path_time, milestones=path)
        return traj

    def visualize_world(self, world):
        vis.add("world", world)
        vis.add("coordinates", coordinates.manager())
        vis.setWindowTitle("Height Map GeneratorSuperclass World")
        vp = vis.getViewport()
        vp.w, vp.h = 600, 600
        vis.setViewport(vp)
        vis.autoFitCamera()
        vis.show()

    def get_world(self, world_file, robot_file, visualize_robot=True):
        world = WorldModel()
        res = world.readFile(world_file)
        if not res:
            raise RuntimeError("Unable to load terrain model")
        if visualize_robot:
            res = world.readFile(robot_file)
            print robot_file
            if not res:
                raise RuntimeError("Unable to load robot model")
            current_config = world.robot(0).getConfig()
            # print current_config
            # time.sleep(100000)
            print len(ProjectConstants.STARTING_CONFIG)
            current_config[:len(ProjectConstants.STARTING_CONFIG)] = ProjectConstants.STARTING_CONFIG
            world.robot(0).setConfig(current_config)
        return world

    @staticmethod
    def validate_config(space, config, silent=True):
        try:
            for i in range(len(config)):
                if not silent:
                    print("Config #" + str(i))
                    print("space.inbounds(end_config):", space.inBounds(config[i]))
                    try:
                        print("space.closed_loop(end_config):", space.closedLoop(config[i]))
                    except AttributeError:
                        pass
                    print("space.isFeasible(end_config)", space.isFeasible(config[i]))
                    print("space.selfCollision(end_config):", space.selfCollision(config[i]))

                if space.inBounds(config[i]) and space.isFeasible(config[i]) and not space.selfCollision(
                        config[i]) and space.closedLoop(config[i]):
                    pass
                else:
                    return False
            return True

        except TypeError:
            if not silent:
                print("space.inbounds(end_config):", space.inBounds(config))
                try:
                    print("space.closed_loop(end_config):", space.closedLoop(config))
                except AttributeError:
                    pass
                print("space.isFeasible(end_config)", space.isFeasible(config))
                print("space.selfCollision(end_config):", space.selfCollision(config))
            if config is None:
                return False
            if space.inBounds(config) and space.isFeasible(config) and not space.selfCollision(
                    config):# and space.closedLoop(config):
                return True
            else:
                return False

    # UR5 Utilities

    def initialize_robot(self):
        # Needs to start ur5.start()
        # and shut_down ur5.stop()
        try:
            ur5 = UR5WithGripperController(ProjectConstants.robot_ip_adress, gripper=False, gravity=[0, 0, 9.8])
        except:
            print "UR5 connection is not initalized!"
            ur5 = None

        return ur5

    @staticmethod
    def ur5_to_klampt(config):
        klampt_config = config[:]
        klampt_config.insert(0, 0.0)
        return klampt_config

    @staticmethod
    def klampt_to_ur5(config):
        return config[1:]

    def vis_world_and_local(self, worldpos, localpos, stop=False):
        wl = []
        for i in localpos:
            w = self.robot.link(ProjectConstants.END_EFFECTOR_IND).getWorldPosition(i)
            wl.append(w)

        t = Trajectory(milestones=worldpos)
        vis.add("world_pos", t)
        vis.setColor("world_pos", 1, 0, 0)
        t2 = Trajectory(milestones=wl)
        vis.add("local_pos", t2)
        vis.setColor("local_pos", 0, 1, 0)
        if stop:
            raw_input()

    @staticmethod
    def motion_planner(qstart, qgoal, gen_space, timed=False):
        settings = {'type': 'sbl',
                    # 'perturbationRadius': np.pi,
                    'perturbationRadius': 0.05,
                    'bidirectional': True,
                    'shortcut': True,
                    'restart': True,
                    'restartTermCond': "{foundSolution:1,maxIters:1000}"
                    }
        #
        # settings = {'type':'fmm'}

        t0 = time.time()
        # print "Creating planner..."
        # Manual construction of planner
        # planner = cspace.MotionPlan(gen_space, type="sbl", connectionThreshold=0.1, shortcut=1)  # accepts keyword arguments
        planner = cspace.MotionPlan(gen_space, **settings)
        # planner.setEndpoints(qstart, qgoal)

        try:
            planner.setEndpoints(qstart, qgoal)
        except ValueError:
            planner.setEndpoints(gen_space.project(qstart), gen_space.project(qgoal))

        # print "Planner creation time", time.time() - t0
        t0 = time.time()
        # print "Planning..."
        if timed:
            while time.time() - t0 < 30:
                planner.planMore(500)
                if planner.getPath() is not None:
                    break
        else:
            numIters = 0
            for round in range(10):
                planner.planMore(500)
                numIters += 1
                if planner.getPath() is not None:
                    break
        # print "Planning time,", numIters, "iterations", time.time() - t0

        path = planner.getPath()
        if path is not None:
            try:
                path = gen_space.ambientspace.discretizePath(gen_space.liftPath(path))
            except AttributeError:
                try:
                    path = gen_space.ambientspace.discretizePath(gen_space.liftPath(path))
                except AttributeError:
                    path = gen_space.discretizePath(path)

            # print "Discretized path has", len(path), "milestones"
        # else:
        #     print "No feasible path was found"
        # provide some debugging information
        V, E = planner.getRoadmap()
        # print len(V), "feasible milestones sampled,", len(E), "edges connected"
        planner.close()
        return path

    def solve_ik(self, space, end_pos):
        robot = self.robot
        max_iter = 1000
            
        end_pos1 = end_pos[:];
        end_pos1[2] += 0.05
        end_pos2 = end_pos[:];
        end_pos2[2] += 0.15
        worldpos = [end_pos1, end_pos2]
        localpos = [[-0.025, 0.310, 0], [-0.025, 0.21, 0]]
        # self.vis_world_and_local(worldpos, localpos, True)

        obj = ik.objective(robot.link(ProjectConstants.END_EFFECTOR_IND), local=localpos, world=worldpos)

        # def feas_check():
        #     config = self.robot.getConfig()
        #     return self.validate_config(space, config, silent=True)
        
        # res = ik.solve_global(obj, iters=max_iter, startRandom=True)#, feasibilityCheck=feas_check)

        if res:
            residuals = ik.residual(obj)
            end_config = robot.getConfig()
            return end_config, self.target_error(localpos, worldpos, debug=False)
        else:
            return None, None

    def generate_trajectory(self, point, grip, current_grip=None):
        init_config = self.robot.getConfig()
        path_time = np.linspace(0, ProjectConstants.ARM_EXECUTION_TIME, len(path)).tolist()
        traj = Trajectory(times=path_time, milestones=path)
        return traj

    @staticmethod
    def generate_space(world, robot):
        collider = WorldCollider(world)
        objs = [ik.fixed_objective(robot.link(0))]
        space = robotcspace.ClosedLoopRobotCSpace(robot, objs, collider)
        space.setup()
        return space

    def get_ik_heap(self, collision_space, qstart, obj_pos, grip, current_grip=None):
        heap = []
        if current_grip is None:
            if grip == "cup_grip":
                for i in range(100):
                    # ADD Classifier for workspace
                    grips = ["cup_side-grip_pos", "cup_side-grip_neg", "cup_top-grip"]
                    grip_mod = grips[i % len(grips)]

                    self.robot.setConfig(qstart)
                    if i < 3:
                        biased = True
                    else:
                        biased = False
                    qgoal, res = self.solve_ik(collision_space, obj_pos, grip_mod, biased)
                    if qgoal is not None and self.validate_config(collision_space, qgoal):
                        diff = [so2.diff(g, s) for g, s in zip(qgoal, qstart)]
                        cost = np.linalg.norm(diff)
                        cost += res
                        heappush(heap, (cost, qgoal))
            else:
                for i in range(200):
                    grips = ["drawer_grip-pos", "drawer_grip-neg"]
                    grip_mod = grips[i % len(grips)]
                    self.robot.setConfig(qstart)
                    qgoal, res = self.solve_ik(collision_space, obj_pos, grip_mod)
                    if qgoal is not None and self.validate_config(collision_space, qgoal):
                        diff = [so2.diff(g, s) for g, s in zip(qgoal, qstart)]
                        cost = np.linalg.norm(diff)
                        cost += res
                        heappush(heap, (cost, qgoal))
        else:
            for i in range(50):
                self.robot.setConfig(qstart)
                qgoal, res = self.solve_ik(collision_space, obj_pos, current_grip)
                if qgoal is not None and self.validate_config(collision_space, qgoal):
                    diff = [so2.diff(g, s) for g, s in zip(qgoal, qstart)]
                    cost = np.linalg.norm(diff)
                    cost += res
                    heappush(heap, (cost, qgoal))
        return heap
